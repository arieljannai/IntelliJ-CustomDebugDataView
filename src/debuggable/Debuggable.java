package debuggable;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;

public class Debuggable {
	public static Method getToStringMethod(Object obj) {
		Stream<Method> sm =
				Arrays.stream(obj.getClass().getDeclaredMethods())
						.filter(m -> m.getName().equals("toDebugString") || m.getName().equals("toString"));
		Map<String, Method> dsm = new HashMap<>();
		sm.forEach(m -> dsm.put(m.getName(), m));

		if (dsm.containsKey("toDebugString")) {
			return dsm.get("toDebugString");
		} else if (dsm.containsKey("toString")) {
			return dsm.get("toString");
		} else {
			try {
				return obj.getClass().getMethod("toDebugString");
			} catch (NoSuchMethodException e) {
				try {
					return obj.getClass().getMethod("toString");
				} catch (NoSuchMethodException e1) {
					return null;
				}
			}
		}
	}

	public static Method getToStringMethod2(Object obj) {
		try {
			return obj.getClass().getDeclaredMethod("toDebugString");
		} catch (NoSuchMethodException e) {
			try {
				return obj.getClass().getDeclaredMethod("toString");
			} catch (NoSuchMethodException e1) {
				try {
					return obj.getClass().getMethod("toDebugString");
				} catch (NoSuchMethodException e2) {
					try {
						return obj.getClass().getMethod("toString");
					} catch (NoSuchMethodException e3) {
						try {
							return Object.class.getClass().getMethod("toString");
						} catch (NoSuchMethodException e4) {
							return null;
						}
					}
				}
			}
		}
	}

	public static String getDebugString(Object obj) {
		try {
			Method m = Debuggable.getToStringMethod2(obj);
			if (m == null) return obj.toString();
			m.setAccessible(true);
			Object o = m.invoke(obj);
			return o.toString();
		} catch (Exception e) {
			return obj.toString();
		}
	}
}