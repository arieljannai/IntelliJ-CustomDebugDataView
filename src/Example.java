public class Example {
	public static void main(String[] args) {
		Test1 test1 = new Test1();
		Test2 test2 = new Test2();
		Test3 test3 = new Test3();

		String s1 = debuggable.Debuggable.getDebugString(test1);
		String s2 = debuggable.Debuggable.getDebugString(test2);
		String s3 = debuggable.Debuggable.getDebugString(test3);

		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);

		System.out.println("\n\nbye bye");
	}
}

/**
 * Has toDebugString() method which will be used
 */
class Test1 {
	private String yo;

	Test1() {
		this.yo = "yo";
	}

	@Override
	public String toString() {
		return "Test1: This is toString overload";
	}

	public String toDebugString() {
		return this.yo + " " + this.yo;
	}
}

/**
 * Has toString() method which will be used
 */
class Test2 {
	private String yo;

	Test2() {
		this.yo = "yo22";
	}

	@Override
	public String toString() {
		return "Test2: This is toString overload 22";
	}
}

/**
 * No toString() / toDebugString() method - Object.toString() will be used
 */
class Test3 {
	private String yo;

	Test3() {
		this.yo = "yo33";
	}
}