# IntelliJ Custom Debug Data View
Allows the developer to set a custom debug string to be used for each data type,
that will be shown near each object while debugging.

## How to use
1. Download the package/class and put it in your `src` folder
2. Start a debugging session
3. Mark a variable in the `Variables` tab and right click on it
4. Choose `Customize Data Views...`
5. Under the `Java Type Renders` tab:
    1. Create a new render type, with a general name, such as `General Object`
    2. Set the value of `Apply renderer to objects of type` to `java.lang.Object`
    3. Under `When rendering a node` select `Use following expression` and enter:
        * The short version: `debuggable.Debuggable.getDebugString(this)`
        * The longer version, which won't fail if you decide not to use the `Debuggable` in some project (recommended):
            * Note: It's because this setting is global in IntelliJ.
        

        ```java
        try {
			Class.forName("debuggable.Debuggable");
			debuggable.Debuggable.getDebugString((Object)this);
		} catch(Exception e) {
			((Object)this).toString();
		}
        ```
        * If you decided to extract it from package or change it's name - fix accordingly. 
        
      ![set_object_data_views][1]

## Example
#### Usage example
```java
/**
 * Has toDebugString() method which will be used
 */
class Test1 {
    private String yo;

    Test1() {
        this.yo = "yo";
    }

    @Override
    public String toString() {
        return "Test1: This is toString overload";
    }

    public String toDebugString() {
        return this.yo + " " + this.yo;
    }
}

/**
 * Has toString() method which will be used
 */
class Test2 {
    private String yo;

    Test2() {
        this.yo = "yo22";
    }

    @Override
    public String toString() {
        return "Test2: This is toString overload 22";
    }
}

/**
 * No toString() / toDebugString() method - Object.toString() will be used
 */
class Test3 {
    private String yo;

    Test3() {
        this.yo = "yo33";
    }
}
```
 
#### Result
![debug_console_screenshot][2]


## Why?
It annoyed me that I don't see what I want in the debugger, and that it's not
comfortable to change it for multiple data types.
So I asked [this question][3] on StackOverflow and then [answered myself][4],
by creating this helper static class.


  [1]: img/set_object_data_views.png
  [2]: img/debug_console_screenshot.png
  [3]: https://stackoverflow.com/q/50722271/2350423
  [4]: https://stackoverflow.com/a/50818990/2350423